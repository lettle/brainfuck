
# 演示代码: ,>,[-<+>].<.
# 演示代码: +++[>++<-].>.
# Hello world代码:
# ++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.

__author__ = "Lettle"
__DEBUG__ = True

int_data = [0]
data_pointer = 0
code_pointer = 0
if_temp = []

code = input("brain: ")

# 逐字解释
def bf_exec(code):
    global code_pointer
    while code_pointer < len(code):
        c = code[code_pointer]
        if   c == "<":
            bf_left()
            code_pointer += 1
        elif c == ">":
            bf_right()
            code_pointer += 1
        elif c == "+":
            bf_add()
            code_pointer += 1
        elif c == "-":
            bf_sub()
            code_pointer += 1
        elif c == ".":
            bf_print()
            code_pointer += 1
        elif c == ",":
            bf_input()
            code_pointer += 1
        elif c == "[":
            bf_jz(code)
        elif c == "]":
            bf_jnz(code)



def bf_showData():
    global int_data, data_pointer
    print('-'*20)
    for i in int_data:
        print(i, end='\t')
    print()
    for i in range(data_pointer-1):
        print("\t", end="")
    print("|")
    print('-' * 20)

def bf_find_jz(code):
    global code_pointer
    p = code_pointer
    code_temp = code[:code_pointer]
    for c in code_temp:
        p -= 1
        if c == "[":
            return p

def bf_find_jnz(code):
    global code_pointer
    p = code_pointer
    code_temp = code[code_pointer:]
    for c in code_temp:
        p += 1
        if c == "]":
            return p

# <     左移指针
def bf_left():
    global data_pointer, __DEBUG__
    data_pointer -= 1
    if __DEBUG__:
        # print("左移指针到:", data_pointer)
        bf_showData()

# >     右移指针
def bf_right():
    global data_pointer, __DEBUG__, int_data
    data_pointer += 1
    if data_pointer == len(int_data):
        int_data.append(0)
    if __DEBUG__:
        # print("右移指针到:", data_pointer)
        bf_showData()

# +     当前数值+1
def bf_add():
    global __DEBUG__, data_pointer, int_data
    int_data[data_pointer] += 1
    if __DEBUG__:
        # print("当前数值加1,", int_data[data_pointer])
        bf_showData()

# -     当前数值-1
def bf_sub():
    global __DEBUG__, data_pointer, int_data
    int_data[data_pointer] -= 1
    if __DEBUG__:
        # print("当前数值减1,", int_data[data_pointer])
        bf_showData()

# .     输出当前指针指向的数值
def bf_print():
    global data_pointer, int_data, __DEBUG__
    prt_int = int_data[data_pointer]
    if not __DEBUG__:
        print(chr(prt_int), end="")
    else:
        print(chr(prt_int))

# ,     输入数值
def bf_input():
    global __DEBUG__, data_pointer, int_data
    show = ""
    if __DEBUG__:
        show = "输入一个数"
    i = int(input(show))
    try:
        int_data[data_pointer] = i
    except IndexError:
        int_data.append(i)
    if __DEBUG__:
        print("当前数值变成: ", int_data[data_pointer])

# [
def bf_jz(code):
    global __DEBUG__, code_pointer, int_data, if_temp, data_pointer
    if int_data[data_pointer] == 0:
        code_pointer = bf_find_jnz(code) + 1
    else:
        if_temp.append(code_pointer)
        code_pointer += 1

# ]
def bf_jnz(code):
    global __DEBUG__, code_pointer, int_data, if_temp, data_pointer
    if int_data[data_pointer] != 0:
        code_pointer = if_temp.pop()
    else:
        code_pointer += 1
        if_temp.pop()

bf_exec(code)
if __DEBUG__:
    bf_showData()